/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | foam-extend: Open Source CFD
   \\    /   O peration     | Version:     5.0
    \\  /    A nd           | Web:         http://www.foam-extend.org
     \\/     M anipulation  | For copyright notice see file Copyright
-------------------------------------------------------------------------------
License
    This file is part of foam-extend.

    foam-extend is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation, either version 3 of the License, or (at your
    option) any later version.

    foam-extend is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with foam-extend.  If not, see <http://www.gnu.org/licenses/>.

\*---------------------------------------------------------------------------*/

#include "pcMultiRockTabulated.H"
#include "addToRunTimeSelectionTable.H"

// * * * * * * * * * * * * * * Static Data Members * * * * * * * * * * * * * //

namespace Foam
{
namespace pcModels
{
    defineTypeNameAndDebug(pcMultiRockTabulated, 0);
    addToRunTimeSelectionTable(pcModel, pcMultiRockTabulated, dictionary);
}
}


// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

Foam::pcModels::pcMultiRockTabulated::pcMultiRockTabulated
(
    const dictionary& dict,
    const phaseInterface& interface
)
:
    pcModel(dict, interface),
    interface_(interface.modelCast<pcModel, relativePhaseInterface>()),
    scale_(dict.lookupOrDefault("scale", 1.0)),
    porous_(interface.fluid().lookupPhase(dict.lookup("porousPhase")))
{
    if (!porous_.pure() && porous_.porous())
    {
        pcs_.setSize(porous_.species().size());
        dPcdSs_.setSize(porous_.species().size());
        Bs_.setSize(porous_.species().size());

        forAll(porous_.species(), i)
        {
            pcs_.set
            (
                i,
                new interpolationTable<scalar>(dict.subDict(porous_.species()[i]))
            );

            const word fieldName(interface.name()+"_"+porous_.species()[i]);

            dPcdSs_.set
            (
                i,
                new surfaceScalarField
                (
                    IOobject
                    (
                        IOobject::groupName("dPcdSf", fieldName),
                        interface.fluid().mesh().time().timeName(),
                        interface.fluid().mesh()
                    ),
                    interface.fluid().mesh(),
                    dimensionedScalar("zero", dimensionSet(1, -1, -2, 0, 0), Zero)
                )
            );

            Bs_.set
            (
                i,
                new surfaceScalarField
                (
                    IOobject
                    (
                        IOobject::groupName("Bf", fieldName),
                        interface.fluid().mesh().time().timeName(),
                        interface.fluid().mesh()
                    ),
                    interface.fluid().mesh(),
                    dimensionedScalar("zero", dimensionSet(1, -1, -2, 0, 0), Zero)
                )
            );
        }
    }
    else
    {
        FatalErrorInFunction
            << "Selected porousPhase: " << porous_.name()
            << " is not a porous and multicomponent phase."
            << exit(FatalError);
    }
}


// * * * * * * * * * * * * * * * * Destructor  * * * * * * * * * * * * * * * //

Foam::pcModels::pcMultiRockTabulated::~pcMultiRockTabulated()
{}


// * * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * //

const Foam::phaseInterface&
Foam::pcModels::pcMultiRockTabulated::interface() const
{
    return interface_;
}


void Foam::pcModels::pcMultiRockTabulated::correct()
{
    const phaseModel& phase = interface_.relative();
    const phaseSystem& fluid = interface_.fluid();
    const volScalarField& alpha = phase;
    const volScalarField& alphaVoid = fluid.alphaVoid();

    surfaceScalarField Seff(fvc::interpolate(alpha/alphaVoid));

    forAll(porous_.Y(), compi)
    {
        surfaceScalarField& dPcdSi = dPcdSs_[compi];
        surfaceScalarField& Bj = Bs_[compi];
 
        forAll(dPcdSi, facei)
        {
            scalar& dpcdS = dPcdSi[facei];
            scalar& B = Bj[facei];
            const scalar& S = Seff[facei];

            // pc = dpcdS*S + B
            scalar pc = scale_*pcs_[compi](S);
            dpcdS = scale_*pcs_[compi].rateOfChange(S);
            B = pc - dpcdS*S;
        }

        surfaceScalarField::GeometricBoundaryField& dPcdSb = dPcdSi.boundaryField();
        surfaceScalarField::GeometricBoundaryField& Bjb = Bj.boundaryField();
        surfaceScalarField::GeometricBoundaryField& Seffb = Seff.boundaryField();

        forAll(phase.U()().boundaryField(), patchi)
        {
            if (!phase.U()().boundaryField()[patchi].fixesValue())
            {
                forAll(dPcdSb[patchi], facei)
                {
                    scalar& dpcdS = dPcdSb[patchi][facei];
                    scalar& B = Bjb[patchi][facei];
                    const scalar& S = Seffb[patchi][facei];

                    // pc = dpcdS*S + B
                    scalar pc = scale_*pcs_[compi](S);
                    dpcdS = scale_*pcs_[compi].rateOfChange(S);
                    B = pc - dpcdS*S;
                }
            }
        }
    }
}


void Foam::pcModels::pcMultiRockTabulated::addTerms
(
    phaseSystem::eqnBlockTable& eqnBlock
)
{
    const phaseModel& phase = interface_.relative();
    const volScalarField& alpha = phase;
    const fvMesh& mesh = interface_.mesh();
    const phaseSystem& fluid = interface_.fluid();
    const volScalarField& alphaVoid = fluid.alphaVoid();

    const label& Ai = phase.blockIndex()[0];

    const word alphaTauName(IOobject::groupName("alphaTaupc", phase.name()));
    const surfaceScalarField& alphaTau = fluid.taus(alphaTauName);

    surfaceScalarField snGradVoid(fvc::snGrad(alphaVoid)*mesh.magSf());
    surfaceScalarField voidf(fvc::interpolate(alphaVoid));

    surfaceScalarField sumYjdPcdS
    (
        IOobject
        (
            IOobject::groupName("sumYjdPcdS", phase.name()),
            mesh.time().timeName(),
            mesh
        ),
        mesh,
        dimensionedScalar("zero", dimensionSet(1, -1, -2, 0, 0), Zero)
    );

    surfaceScalarField sumGradYjdPcdS
    (
        IOobject
        (
            IOobject::groupName("sumGradYjdPcdS", phase.name()),
            mesh.time().timeName(),
            mesh
        ),
        mesh,
        dimensionedScalar("zero", dimensionSet(1, 0, -2, 0, 0), Zero)
    );

    surfaceScalarField sumBjGradYj
    (
        IOobject
        (
            IOobject::groupName("sumBjGradYj", phase.name()),
            mesh.time().timeName(),
            mesh
        ),
        mesh,
        dimensionedScalar("zero", dimensionSet(1, 0, -2, 0, 0), Zero)
    );

    forAll(porous_.Y(), compi)
    {
        const volScalarField& porous = porous_;
        volScalarField Yj(porous_.Y()[compi]/max(porous, 0.001));
        surfaceScalarField Yjf(fvc::interpolate(Yj));
        surfaceScalarField snGradYj(fvc::snGrad(Yj)*mesh.magSf());

        sumYjdPcdS += Yjf*dPcdSs_[compi];
        sumGradYjdPcdS += snGradYj*dPcdSs_[compi];
        sumBjGradYj += Bs_[compi]*snGradYj;
    }

    surfaceScalarField gamma(alphaTau*sumYjdPcdS/voidf);
    surfaceScalarField omega(alphaTau*(snGradVoid*sumYjdPcdS/voidf -sumGradYjdPcdS)/voidf);

    fvScalarMatrix pcEqn
    (
        fvm::laplacian(gamma, alpha)
      - fvm::div(omega, alpha)
      + fvc::div(alphaTau*sumBjGradYj)
    );

    // alpha in alphaEqn [Ai, Ai] (already set)
    eqnBlock[Ai][Ai] += pcEqn;

    // alpha in pEqn [p, Ai] (already set)
    eqnBlock[0][Ai] += pcEqn;
}


// ************************************************************************* //
