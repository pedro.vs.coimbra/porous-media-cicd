# Porous Media

## About
This repository contains a set of computational codes developed by WIKKI Brasil to simulate porous media applications. Version 5.0 of foam-extend as the basis to implemented codes.

### CoupledMatrixFoam
By Roberto Lange and Gabriel Marcos Magalhães, engineers at Wikki Brasil.

*CoupledMatrixFoam* is a new OpenFOAM application to simulate multiphase flows in porous media. The mathematical and numerical formulation is based on OpenFoam's multiphaseEulerFoam application, which models all phases of the physical system using the Euler-Euler methodology for multiphase flows. This formulation is combined with Darcy’s law for flows through porous media and is solved in a coupled fully implicit linear system of equations. This development is based on the framework Foam-extend version 5.0, taking advantage of the most recent *fvBlockMatrix* development for coupled solvers.

The developers implemented a completely implicit approach to enhance the code's robustness for challenging problems involving porous media.

This approach couples the phase fractions and the pressure solution, which improves the solver's stability and allows higher time steps. The structure used to solve the system is based on the latest development aimed at coupled solutions in foam-extend (fvBlockMatrix). It is important to note that the developers based this approach on the linearization of non-linear terms to obtain and solve a linear system.

The Euler-Euler approach models all phases of the physical system, including the porous medium. The developers implemented a particular phase type considering that the porous medium is a stationary phase that composes the system. The treatment of the Darcy term and the capillary pressure term is implicit, enabling higher advances in time, especially in cases where the capillary pressure is pronounced.

## Documentation

Article and user guide are in progress.
Preliminary notes can be found in [docs/mathematicalFormulation](./docs/mathematicalFormulation.md)

## Requirements

- Foam-extend-5.0

Python libs used in post-process scripts:
- pandas
- numpy
- matplotlib

