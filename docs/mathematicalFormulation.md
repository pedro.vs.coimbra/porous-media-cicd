# CoupledMatrixFoam Notes
By Roberto Lange and Gabriel Marcos Magalhães, engineers at Wikki Brasil.

These notes are about a new OpenFOAM application to simulate multiphase flows in porous media. *CoupledMatrixFoam* combines the Eulerian multi-fluid formulation for a system of phase fractions with Darcy’s law for flows through porous media and has a coupled fully implicit solution for the linear system of equations. This development is based on the framework Foam-extend version 5.0, taking advantage of the most recent *fvBlockMatrix* developments for coupled solvers.


## Mathematical Model

#### General equations

In the context of porous media flows, the momentum balance equation of each phase fraction reduces to Darcy’s law.
```math
\alpha_i U_i = - \frac{k_{r,i}}{\mu_i} \mathbf{K} \cdot  ( \nabla p  - \rho_i g) \tag{1}
```
Considering a system of incompressible phases, the mass balance equation for each phase is given as:
```math
\frac{\partial \alpha_i}{\partial t} +  \nabla \cdot \left( \alpha_i \mathbf{U}_i \right) = 0 \tag{2}
```
Substituting Eq. (1) in Eq. (2):
```math
\frac{\partial \alpha_i}{\partial t} +  \nabla \cdot \left( - \frac{k_{r,i}}{\mu_i} \mathbf{K} \cdot  ( \nabla p  - \rho_i g) \right) = 0 \tag{3}
```
Eq. (3) is the final equation for the transport of each phase fraction. Note that the velocity fields were removed and it's no more necessary to solve them.  Another important concept is the mixture velocity $`\mathbf{U}_m`$ which, for *n* phases, is given by
```math
\mathbf{U}_m = \sum_{i=1}^n \alpha_i \mathbf{U}_i \tag{4}
```
The mass conservation of the system can be defined using the previous concept. Again, considering a system of incompressible phases:
```math
\nabla \cdot \left( \mathbf{U}_m \right) = 0 \tag{5}
```

```math
\nabla \cdot \left( - \frac{k_{r,i}}{\mu_i} \mathbf{K} \cdot  ( \nabla p  - \rho_i g) \right) = 0  \tag{6}
```
Usually in OpenFOAM codes, Eq. (6) is the "pressure equation" of the system. In summary, the mathematical model for incompressible multiphase flows in porous media, explained above, consists of a set of partial differential equations, namely the momentum balance equation for each phase fraction, Eq. (1), a total mass balance equation, Eq. (6), and the transport equation of each phase fraction, Eq. (3).

#### Relative permeability effects

A generic framework for multiphase flows with any number of phases requires relative permeability models with respect to each interface fluid-rock. We apply the widely used Brooks and Corey model, which relates the relative permeability of each phase to the phase fraction by
```math
k_{r,i} = k_{r,i (max)} \left( \frac{\alpha_i}{\alpha_v} \right)^\eta \tag{7}
```
The *CoupledMatrixFoam* also allows for the use of tabulated methodology, considering a user-provided table with the relative permeability value as a function of the saturation.
Substituting Eq. (7) in Eq. (6) and removing some terms, yields:
```math
\frac{k_{r,i(max)}}{\mu_i} \left( \frac{\alpha_i}{\alpha_v} \right)^\eta \mathbf{K} \cdot   \nabla p \tag{8}
```
Thereby, a non-linear term between pressure and phase fraction appears in the system of equations and needs special treatment.

#### Linearization by Keser

Despite the coupled formulation, the developed solver works with a linear system of equations. In the mathematical formulation, some terms are non-linear so, it is necessary to linearize them to construct the system of equations.
It is possible to use different approaches but in the current work, the linearisation presented by Keser et al. was used in all non-linear terms.

To understand the methodology proposed by Keser the linearisation is presented in a modified phase continuity equation for a three-phase system:
```math
\dfrac{\partial \alpha_i}{\partial t} + \nabla \cdot (\bar{U}_i \alpha_i) +
\nabla \cdot \left(\alpha_i \sum_{j=1,j \neq i}^{3} \alpha_j(\bar{U}_i - \bar{U}_j) \right) = 0
\tag{9}
```
where $`\alpha_i`$ is the phase fraction of a phase $`i`$ and $`\bar{U}_i`$ is the mean velocity of the same phase.

When $`i=1`$ the non-linear term in the balance equation is given by:
```math
\alpha_1 \sum_{j=2}^{3} \alpha_j (\bar{U}_1 - \bar{U}_j) = 
\alpha_1 \alpha_2 (\bar{U}_1 - \bar{U}_2) + \alpha_1 \alpha_3 (\bar{U}_1 - \bar{U}_3),
\tag{10}
```
where  $`(\bar{U}_1 - \bar{U}_2)`$ is called relative velocity and represented as $`\bar{U}_{r,1,2}`$.

On the phase continuity equation, the phase velocities are not functions of phase fractions
```math
\bar{U}_{r,1,2} \neq f(\alpha_1,\alpha_2).
```

Therefore, the linearisation of the term containing the relative velocities around the solution from the previous iteration is given by:
```math
\bar{U}_{r,1,2} \alpha_1^n  \alpha_2^n \approx \bar{U}_{r,1,2} \alpha_1^0  \alpha_2^0 +
\left( \dfrac{\partial (\bar{U}_{r,1,2} \alpha_1 \alpha_2)}{\partial \alpha_1} \right)^0 (\alpha_1^n - \alpha_1^0) \\
+\left( \dfrac{\partial (\bar{U}_{r,1,2} \alpha_1 \alpha_2)}{\partial \alpha_2} \right)^0 (\alpha_2^n - \alpha_2^0)
```

```math
\approx
\bar{U}_{r,1,2} \alpha_1^0 \alpha_2^0 + \bar{U}_{r,1,2} \alpha_2^0 (\alpha_1^n - \alpha_1^0) + 
 \bar{U}_{r,1,2} \alpha_1^0 (\alpha_2^n - \alpha_2^0)  
\tag{11}
```

```math
\approx
\bar{U}_{r,1,2} \alpha_1^n \alpha_2^0 +  \bar{U}_{r,1,2} \alpha_1^0 \alpha_2^n + \bar{U}_{r,1,2} \alpha_1^0 \alpha_2^0
\tag{12}
```

Following this approach, the non-linear term in the phase fraction balance equation for a general multi-phase system can be written as
```math
\alpha_i \sum_{j=1,j \neq i}^{3} \alpha_j(\bar{U}_i - \bar{U}_j)  = 
\underbrace{\alpha_i^n \sum_{j=1,j \neq i}^{3} \alpha_j^o (\bar{U}_i - \bar{U}_j)}_{implicit} -
\underbrace{\alpha_i^o \sum_{j=1,j \neq i}^{3} \alpha_j^o (\bar{U}_i - \bar{U}_j)}_{explicit} \\
+\underbrace{\alpha_i^o \sum_{j=1,j \neq i}^{3} \alpha_j^n (\bar{U}_i - \bar{U}_j)}_{cross-coupling}.
\tag{13}
```

It is possible to observe that the linearisation method proposed by Keser consists of dividing the non-linear term into three components: explicit, implicit, and cross-coupling. At the converged solution, the explicit and implicit parts will be canceled and just the cross-coupling term is used to model the non-linear term.



#### Capillarity effects
The discontinuity between two moving phases of a multiphase flow on porous media can generate an additional relationship between pressure fields denominated as capillary pressure $`p_c`$, given by:
```math
p_{c,ki} = p_k - p_i  \tag{14}
```
in which $`k`$ is a reference phase and $`i`$ is any other phase of the system.
```math
\alpha_i U_i = - \frac{k_{r,i}}{\mu_i} \mathbf{K} \cdot  ( \nabla p_k - \nabla p_{c,ki}  - \rho_i g) \tag{15}
```
Therefore, the pressure of any phase $`i`$ is defined as a function of the pressure of the reference phase $`k`$ and the capillary pressure of the pair $`ki`$. Note that for the reference phase, the capillary pressure satisfies $`p_{c,kk} = 0`$, and hence Eq. (1) does not change.

A useful approximation  is to consider that $`p_c`$ depends only on saturation, and hence
```math
\nabla p_{c,ki} = \frac{\partial p_{c,ki}}{\partial S_{i}} \nabla S_{i}  \tag{16}
```
We don't solve saturation, so, one more manipulation is necessary.
```math
\nabla p_{c,ki} = \frac{\partial p_{c,ki}}{\partial S_{i}} \left(\frac{\nabla \alpha_{i}}{\alpha_v} - \frac{\alpha_{i} \nabla \alpha_{v}}{\alpha_{v}^{2}} \right) \tag{17}
```

In this way, we can add the capillarity effect without adding new variables to the system of equations. However, the capillarity models must be able to provide the derivative of the capillary pressure in function of saturation.


## Numerical Formulation

### Pressure and phase fraction linearization

Introducing the assistant variable $`\mathbf{T}_i`$:

```math
\mathbf{T}_i = \frac{k_{r,i}}{\mu_i \alpha_i} \mathbf{K}  \tag{18}
```
That way, our transport equation for the phase fraction takes the following form:

```math
\frac{\partial \alpha_i}{\partial t} +  \nabla \cdot \left( - \alpha_i \mathbf{T}_i \cdot  ( \nabla p  - \rho_i g) \right) = 0 \tag{19}
```

Since the solution algorithm uses a linear solver, the non-linear terms need to be linearized. Eq. (8) shows a non-linear term between pressure and phase fraction, this term can be manipulated in two ways using Keser's linearization.  The first way assumes $`\mathbf{T}_i`$ as constant, i.e. $`\mathbf{T}_i \neq f(\alpha_i, \nabla p )`$, but being calculated explicitly with the solution from the previous time-step/iteration. Therefore, the non-linear term between pressure and phase fraction can now be easily linearized.

```math
 \alpha_i^n \mathbf{T}_i^n \cdot   \nabla p^n \approx \mathbf{T}_i^o (\alpha_i^{n}  \cdot   \nabla p^o + \alpha_i^{o}  \cdot   \nabla p^n - \alpha_i^{o}  \cdot   \nabla p^o)   \tag{20}
```
Where superscripts $`n`$ and $`o`$ denote the new and the old time step/iteration.

The second way is considering $`\mathbf{T}_i`$ implicitly. This makes changes to Taylor series expansion because the relative permeability model is highly dependent on phase fraction values.  The Brooks and Corey model, Eq. (7), is a classical model and a good example. The term $`\mathbf{T}_i`$ for this relative permeability model behaves like a function of phase fraction with $`\eta-1`$ exponent,  i.e. $`\mathbf{T}_i = f(\alpha_i^{\eta-1})`$. Assuming that we can approximate $`\mathbf{T}_i`$ by a function with this behavior, our linearization takes the following form:

```math
\alpha_i^n \mathbf{T}_i^n  \cdot   \nabla p^n \approx \mathbf{T}_i^o (\eta \alpha_i^{n}  \cdot   \nabla p^o + \alpha_i^{o}  \cdot   \nabla p^n - \eta \alpha_i^{o}  \cdot   \nabla p^o)   \tag{21}
```

### Gravity contribution

The phase transport equations consider the gravity effects, so, we need to return to Eq. (19) and treat the gravity term. 

```math
\alpha_i \rho_i \mathbf{T}_i \cdot  \mathbf{g} \tag{22}
```
The simplest way would be to add this effect explicitly, but this can slow down performance. A simple linearization was proposed here, only depending on the phase fraction.

```math
\alpha_i^n \rho_i^n\mathbf{T}_i^n \cdot  \mathbf{g} \approx \alpha_i^n \rho_i^o\mathbf{T}_i^o \cdot \mathbf{g} \tag{23}
```

### Capillarity models

The transport equation for each phase fraction with capillary pressure takes the following form:

```math
\frac{\partial \alpha_i}{\partial t} +  \nabla \cdot \left( - \alpha_i \mathbf{T}_i \cdot  ( \nabla p_k - \nabla p_{c,ki} - \rho_i g) \right) = 0 \tag{24}
```

Substituting Eq. (17) in Eq. (24) and rearranging, the capillary pressure term yields:
```math
\alpha_i \frac{\partial p_{ci}}{\partial S_{i}}\mathbf{T}_i  \cdot\left(\frac{\nabla \alpha_{i}}{\alpha_v} - \frac{\alpha_{i} \nabla \alpha_{v}}{\alpha_{v}^{2}} \right) \tag{25}
```
There are two ways to linearize the first term:
```math
\alpha_i \frac{\partial p_{ci}}{\partial S_{i}}\mathbf{T}_i \cdot\frac{\nabla \alpha_{i}}{\alpha_v} \approx \alpha_i^o \frac{\partial p_{ci}}{\partial S_{i}} \mathbf{T}_i^o \cdot\frac{\nabla \alpha_{i}^n}{\alpha_v^o}  \tag{26}
```
```math
\alpha_i \frac{\partial p_{ci}}{\partial S_{i}} \mathbf{T}_i  \cdot\frac{\nabla \alpha_{i}}{\alpha_v} \approx \alpha_i^o \frac{\partial p_{ci}}{\partial S_{i}} \mathbf{T}_i^o \cdot\frac{\nabla \alpha_{i}^n}{\alpha_v^o}  + \alpha_i^n \frac{\partial p_{ci}}{\partial S_{i}} \mathbf{T}_i^o\cdot\frac{\nabla \alpha_{i}^o}{\alpha_v^o}
\\ 
-\alpha_i^o \frac{\partial p_{ci}}{\partial S_{i}} \mathbf{T}_i^o\cdot\frac{\nabla \alpha_{i}^o}{\alpha_v^o} \tag{27}
```
Linearization for the second term:
```math
-\alpha_i \alpha_i \frac{\partial p_{ci}}{\partial S_{i}} \mathbf{T}_i \cdot\left(\frac{\nabla \alpha_{v}}{\alpha_{v}^{2}} \right)
\approx -\alpha_i^n \alpha_i^o \frac{\partial p_{ci}}{\partial S_{i}}\mathbf{T}_i^o \cdot \left(\frac{\nabla \alpha_{v}}{\alpha_{v}^{2}} \right)
\tag{28}
```

